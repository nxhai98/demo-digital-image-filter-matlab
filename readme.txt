Các phép lọc ảnh
	- trên miền không gian:
		+ lọc trung bình
		+ lọc gausian
		+ lọc trung vị
		+ bộ lọc làm nét ảnh (các phép đạo hàm): laplacian, gradient sobel
	- trên miền tần số:
		+ các bộ lọc thông thấp (ideal, gaussian, butterworth)
		+ các bộ lọc thông cao (ideal, gaussian, butterworth)
		+ lọc đồng hình (homomorphic)