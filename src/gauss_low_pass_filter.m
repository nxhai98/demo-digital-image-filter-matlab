function [image_filtered,FT_img, H, U, V] = gauss_low_pass_filter(image_noise,D0)
    [M, N] = size(image_noise);
    
    % Getting Fourier Transform of the input_image 
    % using MATLAB library function fft2 (2D fast fourier transform) 
    FT_img = fft2(double(image_noise)); 

    % Designing filter 
    u = 0:(M-1); 
    v = 0:(N-1); 
    idx = find(u > M/2); 
    u(idx) = u(idx) - M; 
    idy = find(v > N/2); 
    v(idy) = v(idy) - N; 

    % MATLAB library function meshgrid(v, u) returns  
    % 2D grid which contains the coordinates of vectors  
    % v and u. Matrix V with each row is a copy of v  
    % and matrix U with each column is a copy of u  
    [V, U] = meshgrid(v, u); 

    % Calculating Euclidean Distance 
    D = sqrt(U.^2 + V.^2); 

    % determining the filtering mask 
    H = exp(-(D.^2) ./(2*D0^2)); 

    % Convolution between the Fourier Transformed  
    % image and the mask 
    G = H.*FT_img; 

    % Getting the resultant image by Inverse Fourier Transform  
    % of the convoluted image using MATLAB library function   
    % ifft2 (2D inverse fast fourier transform)    
    image_filtered = real(ifft2(double(G)));
    FT_img = fftshift(log(1 + abs(FT_img)));
end

