function [gr, mask, im_raw] = laplacian(image, isotropic)
    mask = 0;
    laplacian_mask=0;
    if isotropic == 45
        mask = [-1 -1 -1; -1 9 -1; -1 -1 -1];
        laplacian_mask = [-1 -1 -1; -1 8 -1; -1 -1 -1];
    elseif  isotropic == 90
        mask = [0 -1 0; -1 5 -1; 0 -1 0];
        laplacian_mask=[0 -1 0; -1 4 -1; 0 -1 0];
    end
    
	gr = image;

	Lint = fix(size(mask,1)/2);
    Pint = fix(size(mask,2)/2);

	% Lines
    for l = Lint+1 : size(image,1)-Lint
        % Pixels
        for p = Pint+1 : size(image,2)-Pint
            % Extract of sub-image (window)
            window = image(l-Lint : l+Lint, p-Pint : p+Pint);
            % convolution between sub-image and mask
            gr(l,p) = sum(sum(double(window) .* mask));
        end
    end
    
    
	im_raw = image;

	Lint = fix(size(laplacian_mask,1)/2);
    Pint = fix(size(laplacian_mask,2)/2);

	% Lines
    for l = Lint+1 : size(image,1)-Lint
        % Pixels
        for p = Pint+1 : size(image,2)-Pint
            % Extract of sub-image (window)
            window = image(l-Lint : l+Lint, p-Pint : p+Pint);
            % convolution between sub-image and mask
            im_raw(l,p) = sum(sum(double(window) .* laplacian_mask));
        end
    end
end