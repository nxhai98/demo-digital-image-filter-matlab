function varargout = laplacian_gui(varargin)
% LAPLACIAN_GUI MATLAB code for laplacian_gui.fig
%      LAPLACIAN_GUI, by itself, creates a new LAPLACIAN_GUI or raises the existing
%      singleton*.
%
%      H = LAPLACIAN_GUI returns the handle to a new LAPLACIAN_GUI or the handle to
%      the existing singleton*.
%
%      LAPLACIAN_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LAPLACIAN_GUI.M with the given input arguments.
%
%      LAPLACIAN_GUI('Property','Value',...) creates a new LAPLACIAN_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before laplacian_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to laplacian_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help laplacian_gui

% Last Modified by GUIDE v2.5 29-Jun-2020 12:43:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @laplacian_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @laplacian_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before laplacian_gui is made visible.
function laplacian_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to laplacian_gui (see VARARGIN)

% Choose default command line output for laplacian_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global isotropic;
isotropic = 45;
setdata(handles, isotropic);

% UIWAIT makes laplacian_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = laplacian_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    contents = cellstr(get(hObject,'String'));
    global isotropic;
    isotropic = str2num(contents{get(hObject,'Value')});
    setdata(handles, isotropic);
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    close;
    main;
    
function setdata(handles, isotropic)
    global image_selected;
    [im, mask, im_raw] = laplacian(image_selected, isotropic);
    axes(handles.axes1);
    imshow((image_selected));
    axes(handles.axes2);
    imshow(im_raw);
    set(handles.uitable1, 'Data', mask);
    axes(handles.axes3);
    imshow(im);
