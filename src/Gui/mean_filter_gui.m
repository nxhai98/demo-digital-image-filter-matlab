function varargout = mean_filter_gui(varargin)
% MEAN_FILTER_GUI MATLAB code for mean_filter_gui.fig
%      MEAN_FILTER_GUI, by itself, creates a new MEAN_FILTER_GUI or raises the existing
%      singleton*.
%
%      H = MEAN_FILTER_GUI returns the handle to a new MEAN_FILTER_GUI or the handle to
%      the existing singleton*.
%
%      MEAN_FILTER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MEAN_FILTER_GUI.M with the given input arguments.
%
%      MEAN_FILTER_GUI('Property','Value',...) creates a new MEAN_FILTER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mean_filter_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mean_filter_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mean_filter_gui

% Last Modified by GUIDE v2.5 12-May-2020 14:50:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mean_filter_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @mean_filter_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mean_filter_gui is made visible.
function mean_filter_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mean_filter_gui (see VARARGIN)

% Choose default command line output for mean_filter_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global mask
global noise
noise = 'gaussian';
mask = 1/4 * [0 1 0; 1 0 1; 0 1 0];
setdata(noise, mask , handles)

% UIWAIT makes mean_filter_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mean_filter_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in back.
function back_Callback(hObject, eventdata, handles)
% hObject    handle to back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close
main

function setdata(noise, mask, handles)
    global image_selected
    axes(handles.axes1)
    imshow((image_selected))
    axes(handles.axes2)
    im_noise = imnoise(image_selected, noise);
    imshow(im_noise)
    set(handles.uitable1, 'Data', mask);
    axes(handles.axes4)
    imshow(mean_filter(im_noise, mask))

% --- Executes on selection change in noise.
function noise_Callback(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(hObject,'String'));
global noise
noise = contents{get(hObject,'Value')};
global mask
setdata(noise, mask, handles)

% Hints: contents = cellstr(get(hObject,'String')) returns noise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noise


% --- Executes during object creation, after setting all properties.
function noise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(hObject,'String'));
mask_text = contents{get(hObject,'Value')};
global mask
global noise
if (strcmp(mask_text, '1 - 4'))
    mask = 1/4 * [0 1 0; 1 0 1; 0 1 0];
end
if (strcmp(mask_text, '1 - 9'))
    mask = 1/9 * [1 1 1; 1 1 1; 1 1 1];
end
if (strcmp(mask_text, '1 - 10'))
    mask = 1/10 * [1 1 1; 1 2 1; 1 1 1];
end
setdata(noise, mask, handles)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
