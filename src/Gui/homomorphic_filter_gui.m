function varargout = homomorphic_filter_gui(varargin)
% HOMOMORPHIC_FILTER_GUI MATLAB code for homomorphic_filter_gui.fig
%      HOMOMORPHIC_FILTER_GUI, by itself, creates a new HOMOMORPHIC_FILTER_GUI or raises the existing
%      singleton*.
%
%      H = HOMOMORPHIC_FILTER_GUI returns the handle to a new HOMOMORPHIC_FILTER_GUI or the handle to
%      the existing singleton*.
%
%      HOMOMORPHIC_FILTER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HOMOMORPHIC_FILTER_GUI.M with the given input arguments.
%
%      HOMOMORPHIC_FILTER_GUI('Property','Value',...) creates a new HOMOMORPHIC_FILTER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before homomorphic_filter_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to homomorphic_filter_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help homomorphic_filter_gui

% Last Modified by GUIDE v2.5 30-Jun-2020 02:42:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @homomorphic_filter_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @homomorphic_filter_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before homomorphic_filter_gui is made visible.
function homomorphic_filter_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to homomorphic_filter_gui (see VARARGIN)

% Choose default command line output for homomorphic_filter_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

yH = 2;
yL = 0.5;
setdata(handles, yH, yL);

% UIWAIT makes homomorphic_filter_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = homomorphic_filter_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    close;
    main;


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    yH = str2double(get(handles.edt_yH, 'String'));
    yL = str2double(get(handles.edt_yL, 'String'));
    setdata(handles, yH, yL);



function edt_yH_Callback(hObject, eventdata, handles)
% hObject    handle to edt_yH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_yH as text
%        str2double(get(hObject,'String')) returns contents of edt_yH as a double


% --- Executes during object creation, after setting all properties.
function edt_yH_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_yH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edt_yL_Callback(hObject, eventdata, handles)
% hObject    handle to edt_yL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edt_yL as text
%        str2double(get(hObject,'String')) returns contents of edt_yL as a double


% --- Executes during object creation, after setting all properties.
function edt_yL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edt_yL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function setdata(handles, yH, yL)
    global image_selected;
    axes(handles.axes1);
    imshow((image_selected));
    [image_filtered, H, U, V] = homomorphic_filter(image_selected, yH, yL);
    axes(handles.axes2);
    mesh(U, V, H);
    axes(handles.axes3);
    imshow(image_filtered);
