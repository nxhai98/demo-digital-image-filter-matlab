function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 12-May-2020 14:40:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global spatial_filter
spatial_filter = {'Mean filter', 'Median filter', 'Gaussian filter', 'Laplacian', 'Gradient Sobel'};
global freq_filter
freq_filter = {'Ideal low-pass filter', 'Ideal high-pass filter', 'Butterworth low-pass filter', 'Butterworth high-pass filter', 'Gaussian low-pass filter', 'Gaussian high-pass filter', 'Homomorphic filter'};
handles.filter.String = spatial_filter;
global filter_selected
if (isempty(filter_selected) == 0)
    filter_selected = 'Mean filter';
end
global image_selected
global img_path
if (isempty(img_path) == 0)
    axes(handles.axes1)
    imshow(image_selected)
    set(handles.edit1,'string',img_path);
end

% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in domain.
function domain_Callback(hObject, eventdata, handles)
% hObject    handle to domain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global filter_selected
    global spatial_filter
    global freq_filter
    contents = cellstr(get(hObject,'String'));
    domain = contents{get(hObject,'Value')};
    if (strcmp(domain, 'spatial domain'))
        handles.filter.String = spatial_filter;
        filter_selected = 'Mean filter';
    end
    if (strcmp(domain, 'frequency domain'))
        handles.filter.String = freq_filter;
        filter_selected = 'Ideal low-pass filter';
    end
% Hints: contents = cellstr(get(hObject,'String')) returns domain contents as cell array
%        contents{get(hObject,'Value')} returns selected item from domain


% --- Executes during object creation, after setting all properties.
function domain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to domain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in filter.
function filter_Callback(hObject, eventdata, handles)
% hObject    handle to filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(hObject,'String'));
global filter_selected
filter_selected = contents{get(hObject,'Value')};
  
% Hints: contents = cellstr(get(hObject,'String')) returns filter contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filter


% --- Executes during object creation, after setting all properties.
function filter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%spatial_filter = {'Mean filter', 'Median filter', 'Gaussian filter', 'Laplacian', 'Gradient Sobel'};
%freq_filter = {'Ideal low-pass filter', 'Ideal high-pass filter', 'Butterworth low-pass filter', 'Butterworth high-pass filter', 'Gaussian low-pass filter', 'Gaussian high-pass filter', 'Homomorphic filter'};

global filter_selected
if (strcmp(filter_selected, 'Mean filter'))
    close
    mean_filter_gui
elseif (strcmp(filter_selected, 'Median filter'))
    close
    median_filter_gui
elseif (strcmp(filter_selected, 'Gaussian filter'))
    close
    gaussian_filter_gui
elseif (strcmp(filter_selected, 'Laplacian'))
    close
    laplacian_gui
elseif (strcmp(filter_selected, 'Gradient Sobel'))
    close
    gradiel_sobel_gui

elseif (strcmp(filter_selected, 'Ideal low-pass filter'))
    close
    ideal_low_pass_filter_gui
elseif (strcmp(filter_selected, 'Ideal high-pass filter'))
    close
    ideal_high_pass_filter_gui
elseif (strcmp(filter_selected, 'Butterworth low-pass filter'))
    close
    butterworth_low_pass_filter_gui
elseif (strcmp(filter_selected, 'Butterworth high-pass filter'))
    close
    butterworth_high_pass_filter_gui
elseif (strcmp(filter_selected, 'Gaussian low-pass filter'))
    close
    gaussian_low_pass_filter_gui
elseif (strcmp(filter_selected, 'Gaussian high-pass filter'))
    close
    gaussian_high_pass_filter_gui 
elseif (strcmp(filter_selected, 'Homomorphic filter'))
    close
    homomorphic_filter_gui
end

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in select_image.
function select_image_Callback(hObject, eventdata, handles)
% hObject    handle to select_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fn, pn] = uigetfile('*.*','select dicom file');
global img_path
img_path = strcat(pn,fn);
set(handles.edit1,'string',img_path);
global image_selected
if (isempty(img_path) == 0)
    image_selected = imread(img_path);
end
axes(handles.axes1)
imshow(image_selected)



% --- Executes during object create, before destroying properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
