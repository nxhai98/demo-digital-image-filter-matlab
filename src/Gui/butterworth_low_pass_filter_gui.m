function varargout = butterworth_low_pass_filter_gui(varargin)
% BUTTERWORTH_LOW_PASS_FILTER_GUI MATLAB code for butterworth_low_pass_filter_gui.fig
%      BUTTERWORTH_LOW_PASS_FILTER_GUI, by itself, creates a new BUTTERWORTH_LOW_PASS_FILTER_GUI or raises the existing
%      singleton*.
%
%      H = BUTTERWORTH_LOW_PASS_FILTER_GUI returns the handle to a new BUTTERWORTH_LOW_PASS_FILTER_GUI or the handle to
%      the existing singleton*.
%
%      BUTTERWORTH_LOW_PASS_FILTER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BUTTERWORTH_LOW_PASS_FILTER_GUI.M with the given input arguments.
%
%      BUTTERWORTH_LOW_PASS_FILTER_GUI('Property','Value',...) creates a new BUTTERWORTH_LOW_PASS_FILTER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before butterworth_low_pass_filter_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to butterworth_low_pass_filter_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help butterworth_low_pass_filter_gui

% Last Modified by GUIDE v2.5 26-May-2020 16:28:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @butterworth_low_pass_filter_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @butterworth_low_pass_filter_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before butterworth_low_pass_filter_gui is made visible.
function butterworth_low_pass_filter_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to butterworth_low_pass_filter_gui (see VARARGIN)

% Choose default command line output for butterworth_low_pass_filter_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global noise
noise = 'gaussian';
global D0
D0 = 50;
global n
n = 1;
setdata(noise, handles, D0, n);
% UIWAIT makes butterworth_low_pass_filter_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = butterworth_low_pass_filter_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    close;
    main;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global noise
    global n
    global D0
    contents = cellstr(get(hObject,'String'));
    noise = contents{get(hObject,'Value')};
    setdata(noise, handles, D0, n)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(~, ~, ~)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, ~, ~)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global D0
    D0 = str2double(get(handles.edit1, 'String'));
    global n
    n = str2double(get(handles.edit2, 'String'));
    global noise
    setdata(noise, handles, D0, n);


function edit2_Callback(~, ~, ~)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, ~, ~)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function setdata(noise, handles, D0, n)
    global image_selected;
    axes(handles.axes1);
    imshow((image_selected));
    axes(handles.axes2);
    im_noise = imnoise(image_selected, noise);
    imshow(im_noise);
    axes(handles.axes3);
    [image_filtered, FT_img, H, U, V] = butterworth_low_pass_filter(im_noise,D0,n);
    mesh(U, V, H);
    axes(handles.axes4);
    imshow(FT_img, []);
    axes(handles.axes5);
    imshow(uint8(image_filtered));
